# /bin/sh
docker build -t localhost/alpine-sshd -f test/config/Dockerfile-sshd test/config
docker build -t localhost/alpine-tunneler -f test/config/Dockerfile-tunneler test/config