import pytest
import docker
import socket
import time
from pytest_network_endpoints.ssh import SshEndpoint
from pytest_network_endpoints.tunnel import TunnelEndpoint


@pytest.fixture(scope="session", autouse=True)
def start_ssh_server(root: SshEndpoint):
    client = docker.from_env()
    sshd = client.containers.run(
        "localhost/alpine-sshd", detach=True, ports={"22/tcp": 2222}
    )
    ip = root.definition["ssh_params"]["hostname"]
    port = root.definition["ssh_params"]["port"]
    wait_socket_open((ip, port))
    yield sshd
    sshd.kill()


@pytest.fixture(scope="session")
def root(endpoints) -> SshEndpoint:
    return endpoints["AlpineRoot"]


@pytest.fixture(scope="session")
def alice(endpoints) -> SshEndpoint:
    return endpoints["AlpineAlice"]


@pytest.fixture(scope="session")
def bob(endpoints) -> SshEndpoint:
    return endpoints["AlpineBob"]


@pytest.fixture(scope="session")
def tunnel(endpoints) -> TunnelEndpoint:
    return endpoints["Tunnel"]


def wait_socket_open(address, timeout=10):
    sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sck_open = 0
    deadline = time.monotonic() + timeout
    while not sck_open and time.monotonic() < deadline:
        sck_open = sck.connect_ex(address)
        time.sleep(0.5)
    sck.close()
    if not sck_open:
        raise TimeoutError
